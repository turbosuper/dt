LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY segment IS
   PORT( clk      : IN std_logic;
         bcd      : IN std_logic_vector(3 DOWNTO 0);       --BCD input
         segment7 : OUT std_logic_vector(6 DOWNTO 0)
         );
END segment;

ARCHITECTURE Behavioral OF segment IS
BEGIN
   PROCESS (clk,bcd)
   BEGIN
      IF (clk'EVENT AND clk='1') THEN
         CASE  bcd IS
         -- 1 = AUS, 0 = AN           6543210 <- Segement
            WHEN "0000"=> segment7 <="1000000";    -- '0'
            WHEN "0001"=> segment7 <="1111001";    -- '1'
            WHEN "0010"=> segment7 <="0100100";    -- '2'
            WHEN "0011"=> segment7 <="0110000";    -- '3'
            WHEN "0100"=> segment7 <="0011001";    -- '4' 
            WHEN "0101"=> segment7 <="0010010";    -- '5'
            WHEN "0110"=> segment7 <="0000010";    -- '6'
            WHEN "0111"=> segment7 <="1111000";    -- '7'
            WHEN "1000"=> segment7 <="0000000";    -- '8'
            WHEN "1001"=> segment7 <="0010000";    -- '9'
            WHEN OTHERS=> segment7 <="1111111";    -- nichts
         END CASE;
      END IF;
   END PROCESS;

END Behavioral;