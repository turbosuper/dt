LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY timer IS
GENERIC (
	ticksforcenti	: integer := 50e4;  --set this to "50" when testing in ModelSim
	secondsneeded		: integer := 3
	);

PORT (
	clk				:in std_logic;
	enable 			:in std_logic;
	reset				:in std_logic;
	timeisout		:out std_logic := '0';
	centiseconds	:out integer;
	seconds			:out integer
	);
END ENTITY;

ARCHITECTURE timeflow OF timer is
 SIGNAL periods	:integer := 0; --Variabel die speichert Anzahl der verlaufene Perioden
 SIGNAL centi		:integer := 0;	--Hundertstelsekunden
 SIGNAL sec			:integer := secondsneeded;	--Sekunden

BEGIN

centiseconds 	<= centi;
seconds 			<= sec;

PROCESS (clk, reset)
	BEGIN
	IF (reset = '1') THEN
		timeisout<= '0';
		periods	<= 0;
		centi		<= 0;
		sec		<= secondsneeded;
		
	ELSIF(clk'event AND clk='0' AND enable='1') THEN
	
			
		IF (periods = ticksforcenti -1) THEN
			periods <= 0;
			IF (centi = 0) THEN
				IF (sec = 0) THEN
					timeisout <= '1';
				ELSE
					sec <= sec -1;
					centi <= 99;
				END IF;
			ELSE
			centi <= centi -1; 
			END IF;
			
		ELSE
			periods <= periods + 1;
		END IF;
	END IF;

END PROCESS;

END timeflow;
