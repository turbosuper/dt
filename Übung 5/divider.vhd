library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Divider is
    Generic ( b  : natural range 4 to 32 := 32 ); -- Breite 
    Port ( start     : in   STD_LOGIC;
           divisor   : in   STD_LOGIC_VECTOR (b-1 downto 0);
           dividend  : in   STD_LOGIC_VECTOR (b-1 downto 0);
           quotient  : out  STD_LOGIC_VECTOR (b-1 downto 0);
           remainder : out  STD_LOGIC_VECTOR (b-1 downto 0);
           busy      : out  STD_LOGIC;
           clk       : in   STD_LOGIC);
end Divider;

architecture Behave_Unsigned of Divider is
signal dd : unsigned(b-1 downto 0); -- dividend
signal dr : unsigned(b-1 downto 0); -- divisor
signal q  : unsigned(b-1 downto 0); -- qoutient
signal r  : unsigned(b-1 downto 0); -- remainder
signal bits : integer range b downto 0;
type zustaende is (idle, prepare, shift, sub, done);
signal z : zustaende;

begin

   process 
   variable diff : unsigned(b-1 downto 0);
   begin
      wait until rising_edge(clk);
      case z is 
         when idle => 
            if (start='1') then 
               z <= prepare; 
               busy <= '1';
            end if;
            dd <= unsigned(dividend);
            dr <= unsigned(divisor);

         when prepare =>
            q    <= (others=>'0');
            r    <= (others=>'0');
            z    <= shift;            
            bits <= b;
            -- Sonderfall: Division durch Null
            if (dr=0) then  
               q <= (others=>'1');
               r <= (others=>'1');
               z <= done;
            -- Sonderfall: Divisor größer als Dividend
            elsif (dr>dd) then 
               r <= dd;
               z <= done;
            -- Sonderfall: Divisor gleich Dividend
            elsif (dr=dd) then
               q <= to_unsigned(1,b);
               z <= done;
            end if;

         when shift =>
            -- erst mal die beiden Operanden 
            -- für die Subtraktion zurechtrücken
            if ( (r(b-2 downto 0)&dd(b-1)) < dr ) then
               bits <= bits-1;
               r    <= r(b-2 downto 0)&dd(b-1);
               dd   <= dd(b-2 downto 0)&'0';
            else
               z    <= sub;
            end if;

         when sub =>
            if (bits>0) then
               r    <= r(b-2 downto 0)&dd(b-1);
               dd   <= dd(b-2 downto 0)&'0';
               -- Rest minus Divisor
               diff := (r(b-2 downto 0)&dd(b-1)) - dr;  
               if (diff(b-1)='0') then                 
                  -- wenn kein Unterlauf 
                  --> Divisor passt noch rein 
                  --> MSB=0 --> 1 in Ergebnis einschieben
                  q <= q(b-2 downto 0) & '1';
                  r <= diff;
               else
                  -- wenn Unterlauf 
                  --> 0 einschieben, mit altem Wert weiterrechnen
                  q <= q(b-2 downto 0) & '0';
               end if;
               bits <= bits-1;
            else
               z    <= done;
            end if;
            
         when done =>
            busy <= '0';
            -- Handshake: wenn nötig warten, bis start='0'
            if (start='0') then 
               z <= idle; 
            end if;
      end case;
   end process;
   
   quotient  <= std_logic_vector(q);
   remainder <= std_logic_vector(r);
   
end;