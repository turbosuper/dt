LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY tb_timer IS
END ENTITY;

ARCHITECTURE test OF tb_timer is

	
COMPONENT timer PORT (
	clk: in std_logic;
	reset: in std_logic;
	timeisout: out std_logic
	);
END COMPONENT;

CONSTANT anzahldertakten : integer := 50e6;
CONSTANT clockperiod : time := 1000ms/anzahldertakten;
	
SIGNAL tbclk : std_logic := '0';
SIGNAL tbreset : std_logic := '0';
SIGNAL tbtimeisout : std_logic;

BEGIN
dut: timer
PORT MAP (
	clk => tbclk,
	reset => tbreset,
	timeisout => tbtimeisout
	);

tbclk <= NOT tbclk AFTER clockperiod/2;
	
END test;