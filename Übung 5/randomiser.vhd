LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY randomiser IS
GENERIC (
	maxrandom : integer := 17
	);

PORT (
	enable : in std_logic; 
	clk: in std_logic;
	getvalue: in std_logic;
	randomvalue: out integer := 0
	);
END ENTITY;

ARCHITECTURE generaterandom OF randomiser is
 SIGNAL periods: integer := 0 ; --Variabel die speichert Anzahl der verlaufene Perioden

 BEGIN

PROCESS (enable, clk)
BEGIN
		IF enable = '1' THEN
		IF(clk'event AND clk='1') THEN
				IF (periods = maxrandom) THEN
					periods <= 0; 
				ELSE
					periods <= periods + 1 ;
				END IF;
		END IF;
		IF (getvalue'event AND getvalue = '1') THEN
			randomvalue <= periods;
		END IF;
	END IF;
	
END PROCESS;

END generaterandom;
