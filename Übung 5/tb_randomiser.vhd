LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY tb_randomiser IS
END ENTITY;

ARCHITECTURE test OF tb_randomiser is

	
COMPONENT randomiser PORT (
	enable : in std_logic; 
	clk: in std_logic;
	getvalue: in std_logic;
	randomvalue: out integer := 0
	);
END COMPONENT;

CONSTANT anzahldertakten : integer := 10e7;
CONSTANT clockperiod : time := 1000ms/anzahldertakten;
	
SIGNAL tbclk : std_logic := '0';
SIGNAL tbenable : std_logic;
SIGNAL tbgetvalue: std_logic := '0' ;
SIGNAL tbrandomvalue: integer;

BEGIN
dut: randomiser
PORT MAP (
	clk => tbclk,
	enable => tbenable,
	getvalue => tbgetvalue,
	randomvalue => tbrandomvalue
	);

tbclk <= NOT tbclk AFTER clockperiod/2;
tbenable <= '0' AFTER 20 ns, '1' AFTER 25 ns, '0' AFTER 45 ns, '1' AFTER 65 ns, '0' AFTER 145 ns, '1' AFTER 165 ns;
tbgetvalue <= NOT tbgetvalue AFTER 30 ns;
	
END test;