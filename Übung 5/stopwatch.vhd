LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY stopwatch IS
GENERIC (
	ticksformilli : integer := 5e4
	);

PORT (
	clk				:in std_logic;
	enable			:in std_logic;
	reset				:in std_logic;
	milliseconds1	:out std_logic_vector(3 DOWNTO 0);
	milliseconds2	:out std_logic_vector(3 DOWNTO 0);
	milliseconds3	:out std_logic_vector(3 DOWNTO 0);
	seconds1			:out std_logic_vector(3 DOWNTO 0);
	seconds2			:out std_logic_vector(3 DOWNTO 0);
	minutes			:out std_logic_vector(3 DOWNTO 0)
	);
END ENTITY;

ARCHITECTURE timeflow OF stopwatch is
 SIGNAL periods		:integer := 0;
 SIGNAL milli1			:integer := 0;	--Millisekunden	LSB
 SIGNAL milli2			:integer := 0;	--						
 SIGNAL milli3			:integer := 0;	--						MSB
 SIGNAL sec1			:integer := 0;	--Sekunden			LSB
 SIGNAL sec2			:integer := 0;	--						MSB
 SIGNAL min				:integer := 0;	--Minuten

BEGIN

milliseconds1 	<= std_logic_vector(to_unsigned(milli1, milliseconds1'length));
milliseconds2 	<= std_logic_vector(to_unsigned(milli2, milliseconds2'length));
milliseconds3 	<= std_logic_vector(to_unsigned(milli3, milliseconds3'length));
seconds1 		<= std_logic_vector(to_unsigned(sec1, seconds1'length));
seconds2			<= std_logic_vector(to_unsigned(sec2, seconds2'length));
minutes			<= std_logic_vector(to_unsigned(min, minutes'length));

PROCESS (clk, reset)
	BEGIN
	IF (reset = '1') THEN
		periods	<= 0;
		milli1	<= 0;
		milli2	<= 0;
		milli3	<= 0;
		sec1		<= 0;
		sec2		<= 0;
		min		<= 0;
		
	ELSIF(clk'event AND clk='0' AND enable='1') THEN
		IF (periods = ticksformilli -1) THEN
			periods <= 0;
			IF (milli1 = 9) THEN
				milli1 <= 0;
				IF (milli2 = 9) THEN
					milli2 <= 0;
					IF (milli3 = 9) THEN
						milli3 <= 0;
						IF (sec1 = 9) THEN
							sec1 <= 0;
							IF (sec2 = 5) THEN
								sec2 <= 0;
								min <= min + 1;
							ELSE
								sec2 <= sec2 + 1;
							END IF;
						ELSE
							sec1 <= sec1 + 1;
						END IF;
					ELSE
						milli3 <= milli3 + 1;
					END IF;
				ELSE
					milli2 <= milli2 + 1;
				END IF;
			ELSE
				milli1 <= milli1 + 1;
			END IF;
		ELSE
			periods <= periods + 1;
		END IF;
	END IF;

END PROCESS;

END timeflow;
