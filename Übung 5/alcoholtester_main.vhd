LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE ieee.numeric_std.ALL;

ENTITY alcoholtester_main IS
   GENERIC( n  : integer := 18);

   PORT( clk, reset, next_step                           : IN std_logic;                        -- reset = KEY0, next_step = KEY3
         SWITCH_in                                       : IN  std_logic_vector (n-1 DOWNTO 0);  -- Schalter
         LEDG0, LEDG1                                    : OUT std_logic;                       -- Info LEDs
         LED_out                                         : OUT std_logic_vector (n-1 DOWNTO 0); -- rote LEDs
         HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7  : OUT std_logic_vector(6 DOWNTO 0)     -- 7-Segement Anzeige
         );
END alcoholtester_main;

ARCHITECTURE LOGIK OF alcoholtester_main IS
   TYPE Zustaende IS (s0, s1, s2, s3, s4, p1, p2, p3, s21, s22, s23, s24, s25, s26, s27, s28, s29, s210, p21, p22, p23, p24, p25, p26, p27, p28, p29, p210);
   
---------------- COMPONENT ------------------------------------------
   
-- 7SEGMENT
	COMPONENT segment IS
      PORT( clk            : IN std_logic;                           -- clock input
            bcd            : IN std_logic_vector(3 DOWNTO 0);        -- BCD input
            segment7       : OUT std_logic_vector(6 DOWNTO 0)        -- Zahl anzeige output
            );
	END COMPONENT;
   
-- TIMER
   COMPONENT timer IS
      PORT( clk            : IN std_logic;                           -- clock input
            enable         : IN std_logic;                           -- 
            reset          : IN std_logic;                           -- Reset Timer
            timeisout      : OUT std_logic;                          -- '1' bei timerende
            seconds			: OUT INTEGER
            );
   END COMPONENT;


-- STOPUHR
   COMPONENT stopwatch IS
      PORT( clk				:in std_logic;
            enable			:in std_logic;
            reset				:in std_logic;
            milliseconds1	:out std_logic_vector(3 DOWNTO 0);
            milliseconds2	:out std_logic_vector(3 DOWNTO 0);
            milliseconds3	:out std_logic_vector(3 DOWNTO 0);
            seconds1			:out std_logic_vector(3 DOWNTO 0);
            seconds2			:out std_logic_vector(3 DOWNTO 0);
            minutes			:out std_logic_vector(3 DOWNTO 0)
            );
   END COMPONENT;
   
   COMPONENT randomiser IS
      PORT( enable         : IN std_logic; 
            clk            : IN std_logic;
            getvalue       : IN std_logic;
            randomvalue    : OUT integer
            );
   END COMPONENT;
   
---------------- SIGNAL ----------------------------------------------

   SIGNAL Zustand : Zustaende;
   
   SIGNAL uebertrag                                               : std_logic_vector(n-1 DOWNTO 0);   -- Zwischeninstanz zu LED_out
   SIGNAL check                                                   : std_logic_vector(n-1 DOWNTO 0);   -- LED und SWITCH gleich?
   SIGNAL seg_0, seg_1, seg_2, seg_3, seg_4, seg_5, seg_6, seg_7  : std_logic_vector(6 DOWNTO 0);     -- für Output in der Entity
   SIGNAL bcd_0, bcd_1, bcd_2, bcd_3, bcd_4, bcd_5, bcd_6, bcd_7  : std_logic_vector(3 DOWNTO 0);     -- für Input 7Segment
   SIGNAL timeisout, reset_timer, en_timer, reset_watch, en_watch, en_random, getvalue : std_logic;                        --
   SIGNAL sec_timer, sec_watch, random                            : integer :=0 ;
   SIGNAL milliseconds1, milliseconds2, milliseconds3, seconds1, seconds2 : std_logic_vector(3 DOWNTO 0);
   
BEGIN

---------------- PORT MAP --------------------------------------------

   HEX_0 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_0,
               segment7    => seg_0
               );
               
   HEX_1 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_1,
               segment7    => seg_1
               );
               
   HEX_2 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_2,
               segment7    => seg_2
               );
               
   HEX_3 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_3,
               segment7    => seg_3
               );
               
   HEX_4 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_4,
               segment7    => seg_4
               );
               
   HEX_5 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_5,
               segment7    => seg_5
               );
               
   HEX_6 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_6,
               segment7    => seg_6
               );
               
   HEX_7 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_7,
               segment7    => seg_7
               );

   timer_0 : timer
   PORT MAP(   clk         => clk,
               enable      => en_timer,
               reset       => reset_timer,
               timeisout   => timeisout,
               seconds     => sec_timer
               );
   
   stopwatch_0 : stopwatch
   PORT MAP(   clk		      => clk,
               enable		   => en_watch,
               reset			   => reset_watch,
               milliseconds1	=> milliseconds1, -- bei 123milSek = 3
               milliseconds2	=> milliseconds2, -- bei 123milSek = 2
               milliseconds3	=> milliseconds3, -- bei 123milSek = 1
               seconds1			=> seconds1,     -- bei 10sek = 0
               seconds2			=> seconds2      -- bei 10sek = 1
               );
            
   random_0 : randomiser
   PORT MAP(   enable      => en_random,
               clk         => clk,
               getvalue    => getvalue,
               randomvalue => random
               );

---------------- LOGIK -----------------------------------------------
   
   HEX0 <= seg_0;
   HEX1 <= seg_1;
   HEX2 <= seg_2;
   HEX3 <= seg_3;
   HEX4 <= seg_4;
   HEX5 <= seg_5;
   HEX6 <= seg_6;
   HEX7 <= seg_7;


   PROCESS(SWITCH_in, clk, reset, timeisout)
      VARIABLE zaehler, Gesamt, ready    :  integer := 0;

   BEGIN
      zaehler := 0;
      en_random <= '1';

      IF(reset = '0') THEN
         Gesamt      := 0;
         en_watch    <= '0';
         en_timer    <= '0';
         reset_watch <= '1';
         reset_timer <= '1';
         bcd_0       <= "0000";
         bcd_1       <= "0000";
         bcd_2       <= "0000";
         bcd_3       <= "0000";
         Zustand     <= s0;
         
      ELSIF (clk'event AND clk = '1') THEN
         FOR i IN 0 TO n-1 LOOP
            check(i)    <= (uebertrag(i) AND SWITCH_in(i));
            IF(check(i) = '1') THEN
               zaehler := zaehler +1;
            END IF;
         END LOOP;
      
         CASE Zustand IS
      
   ---------------- STAGE 0 (START) ----------------
            WHEN s0 =>
               uebertrag <= "000000000000000000";
               LED_out <= uebertrag;
               reset_timer <= '0';
            
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s1;
               ELSE
                  Zustand <= s0;
               END IF;
      
   -------------------- STAGE 1 --------------------
            WHEN s1 =>
               reset_timer <= '0';
               en_timer    <= '1';
               IF(timeisout = '0') THEN
                               -- 765432109876543210 --
                  uebertrag   <= "010001010100010000";
                  LED_out     <= uebertrag;
                  LEDG1       <= '0';
               ELSE
                  Zustand  <= p1;
                  LEDG1    <= '1';
                  en_timer <= '0';
                  Gesamt   := Gesamt + zaehler;
               END IF;
      
   -------------------- PAUSE 1 --------------------
            WHEN p1 =>
               uebertrag   <= "000000000000000000";
               LED_out     <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  reset_timer <= '1';
                  Zustand     <= s2;
               ELSE
                  Zustand     <= p1;
               END IF;
      
   -------------------- STAGE 2 --------------------
            WHEN s2 =>
               reset_timer <= '0';
               en_timer    <= '1';
               IF(timeisout = '0') THEN
                              -- 765432109876543210 --
                  uebertrag  <= "100010000100000101";
                  LED_out <= uebertrag;
                  LEDG1 <= '0';
               ELSE
                  Zustand  <= p2;
                  LEDG1    <= '1';
                  en_timer <= '0';
                  Gesamt   := Gesamt + zaehler;
               END IF;
      
   -------------------- PAUSE 2 --------------------
            WHEN p2 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  reset_timer <= '1';
                  Zustand <= s3;
               ELSE
                  Zustand <= p2;
               END IF;
      
   -------------------- STAGE 3 --------------------
            WHEN s3 =>
               reset_timer <= '0';
               en_timer    <= '1';
               IF(timeisout = '0') THEN
                              -- 765432109876543210 --
                  uebertrag  <= "001010001010000010";
                  LED_out  <= uebertrag;
                  LEDG1    <= '0';
               ELSE
                  Zustand  <= p3;
                  LEDG1    <= '1';
                  en_timer <= '0';
                  Gesamt   := Gesamt + zaehler;
               END IF;
      
   -------------------- PAUSE 3 --------------------
            WHEN p3 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  reset_timer <= '1';
                  Zustand <= s21;
               ELSE
                  Zustand <= p3;
               END IF;
      
   ------------------- STAGE 2.1 -------------------
            WHEN s21 =>
               reset_watch <= '0';
               en_watch <= '1';
               getvalue <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     Zustand <= p21;
                     getvalue    <= '0';
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.1 --------------------
            WHEN p21 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s22;
               ELSE
                  Zustand <= p21;
               END IF;
               
   ------------------- STAGE 2.2 -------------------
            WHEN s22 =>
               getvalue <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     Zustand <= p22;
                     getvalue    <= '0';
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.2 --------------------
            WHEN p22 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s23;
               ELSE
                  Zustand <= p22;
               END IF;
               
   ------------------- STAGE 2.3 -------------------
            WHEN s23 =>
               getvalue <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     Zustand <= p23;
                     getvalue    <= '0';
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.3 --------------------
            WHEN p23 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s24;
               ELSE
                  Zustand <= p23;
               END IF;
               
   ------------------- STAGE 2.4 -------------------
            WHEN s24 =>
               getvalue <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     Zustand <= p24;
                     getvalue    <= '0';
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.4 --------------------
            WHEN p24 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s25;
               ELSE
                  Zustand <= p24;
               END IF;
               
   ------------------- STAGE 2.5 -------------------
            WHEN s25 =>
               getvalue <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     Zustand <= p25;
                     getvalue    <= '0';
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.5 --------------------
            WHEN p25 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s26;
               ELSE
                  Zustand <= p25;
               END IF;
               
   ------------------- STAGE 2.6 -------------------
            WHEN s26 =>
               getvalue <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     Zustand <= p26;
                     getvalue    <= '0';
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.6 --------------------
            WHEN p26 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s27;
               ELSE
                  Zustand <= p26;
               END IF;
               
   ------------------- STAGE 2.7 -------------------
            WHEN s27 =>
               getvalue <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     getvalue    <= '0';
                     Zustand <= p27;
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.7 --------------------
            WHEN p27 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s28;
               ELSE
                  Zustand <= p27;
               END IF;
               
   ------------------- STAGE 2.8 -------------------
            WHEN s28 =>
               getvalue <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     Zustand <= p28;
                     getvalue <= '0';
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.8 --------------------
            WHEN p28 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s29;
               ELSE
                  Zustand <= p28;
               END IF;
               
   ------------------- STAGE 2.9 -------------------
            WHEN s29 =>
               getvalue <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     Zustand <= p29;
                     getvalue <= '0';
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.9 --------------------
            WHEN p29 =>
               uebertrag  <= "000000000000000000";
               LED_out <= uebertrag;
               
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand <= s210;
               ELSE
                  Zustand <= p29;
               END IF;
               
   ------------------- STAGE 2.10 -------------------
            WHEN s210 =>
               getvalue          <= '1';
               uebertrag  <= "000000000000000000";
               uebertrag(random) <= '1';
               LED_out <= uebertrag;
               
               IF(uebertrag(random) = '1') THEN
                  IF(SWITCH_in = uebertrag) THEN
                     Zustand <= P210;
                     ready := 1;
                     getvalue    <= '0';
                     LEDG1 <= '1';
                  ELSE
                     LEDG1 <= '0';
                  END IF;
               END IF;
         
   -------------------- PAUSE 2.10 --------------------
            WHEN p210 =>
               en_watch    <= '0';
               
               uebertrag   <= "000000000000000000";
               LED_out     <= uebertrag;
               
         
   -------------------- ENDE --------------------
            WHEN OTHERS => Zustand <= s0;
         END CASE;
         
         IF(en_timer = '1') THEN
            bcd_0 <= std_logic_vector(to_unsigned(sec_timer, bcd_0'length));  -- Timer anzeige an HEX0
            bcd_4 <= std_logic_vector(to_unsigned(zaehler, bcd_4'length));    -- Zaehler anzeige an HEX4
         ELSIF((en_watch = '1') OR (ready = 1)) THEN
            bcd_0 <= milliseconds2;
            bcd_1 <= milliseconds3;
            bcd_2 <= seconds1;
            bcd_3 <= seconds2;
            bcd_4 <= "0000";
         ELSE
            bcd_0 <= "0000";
            bcd_1 <= "0000";
         END IF;
         
         CASE Gesamt IS
            WHEN 0   => bcd_6 <= "0000";
                        bcd_7 <= "0000";
            WHEN 1   => bcd_6 <= "0001";
                        bcd_7 <= "0000";
            WHEN 2   => bcd_6 <= "0010";
                        bcd_7 <= "0000";
            WHEN 3   => bcd_6 <= "0011";
                        bcd_7 <= "0000";
            WHEN 4   => bcd_6 <= "0100";
                        bcd_7 <= "0000";
            WHEN 5   => bcd_6 <= "0101";
                        bcd_7 <= "0000";
            WHEN 6   => bcd_6 <= "0110";
                        bcd_7 <= "0000";
            WHEN 7   => bcd_6 <= "0111";
                        bcd_7 <= "0000";
            WHEN 8   => bcd_6 <= "1000";
                        bcd_7 <= "0000";
            WHEN 9   => bcd_6 <= "1001";
                        bcd_7 <= "0000";
            WHEN 10  => bcd_6 <= "0000";
                        bcd_7 <= "0001";
            WHEN 11  => bcd_6 <= "0001";
                        bcd_7 <= "0001";
            WHEN 12  => bcd_6 <= "0010";
                        bcd_7 <= "0001";
            WHEN 13  => bcd_6 <= "0011";
                        bcd_7 <= "0001";
            WHEN 14  => bcd_6 <= "0100";
                        bcd_7 <= "0001";
            WHEN 15  => bcd_6 <= "0101";
                        bcd_7 <= "0001";
            WHEN 16  => bcd_6 <= "0110";
                        bcd_7 <= "0001";
            WHEN 17  => bcd_6 <= "0111";
                        bcd_7 <= "0001";
            WHEN 18  => bcd_6 <= "1000";
                        bcd_7 <= "0001";
            WHEN 19  => bcd_6 <= "1001";
                        bcd_7 <= "0001";
            WHEN 20  => bcd_6 <= "0000";
                        bcd_7 <= "0010";
            WHEN 21  => bcd_6 <= "0001";
                        bcd_7 <= "0010";
            WHEN 22  => bcd_6 <= "0010";
                        bcd_7 <= "0010";
            WHEN 23  => bcd_6 <= "0011";
                        bcd_7 <= "0010";
            WHEN 24  => bcd_6 <= "0100";
                        bcd_7 <= "0010";
            WHEN 25  => bcd_6 <= "0101";
                        bcd_7 <= "0010";

            WHEN OTHERS => bcd_6 <= "1111";
                           bcd_7 <= "1111";
         END CASE;
      END IF;
   END PROCESS;   
END LOGIK;
