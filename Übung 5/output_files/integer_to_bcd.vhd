LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY integer_to_bcd IS
   PORT( input : IN integer;
         clk   : IN std_logic;
         bcd   : OUT std_logic_vector(3 DOWNTO 0)
         );
END integer_to_bcd;

ARCHITECTURE LOGIK OF integer_to_bcd IS
   COMPONENT segment IS
      PORT( clk            : IN std_logic;                           -- clock input
            bcd            : IN std_logic_vector(3 DOWNTO 0);        -- BCD input
            segment7       : OUT std_logic_vector(6 DOWNTO 0)        -- Zahl anzeige output
            );
	END COMPONENT;
   
BEGIN
   HEX : segment
   PORT MAP(   clk   => clk,
               bcd   => bcd
               );
      
   PROCESS (clk,bcd)
   BEGIN
      IF (clk'EVENT AND clk='1') THEN
         CASE  input IS
            WHEN 0 => bcd <= "0000";
            WHEN 1 => bcd <= "0001";
            WHEN 2 => bcd <= "0010";
            WHEN 3 => bcd <= "0011";
            WHEN 4 => bcd <= "0100";
            WHEN 5 => bcd <= "0101";
            WHEN 6 => bcd <= "0110";
            WHEN 7 => bcd <= "0111";
            WHEN 8 => bcd <= "1000";
            WHEN 9 => bcd <= "1001";
            WHEN OTHERS => bcd <= "1111";
         END CASE;
      END IF;
   END PROCESS;
END LOGIK;

