LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY test1 IS
   GENERIC( n  : integer := 18);

   PORT( clk, reset, next_step                           : IN std_logic;                        -- reset = KEY0, next_step = KEY3
         SWITCH_in                                       : IN  std_logic_vector (n-1 DOWNTO 0);  -- Schalter
         LEDG0, LEDG1                                    : OUT std_logic;                       -- Info LEDs
         LED_out                                         : OUT std_logic_vector (n-1 DOWNTO 0); -- rote LEDs
         HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7  : OUT std_logic_vector(6 DOWNTO 0)     -- 7-Segement Anzeige
         );
END test1;

ARCHITECTURE LOGIK OF test1 IS
   TYPE Zustaende IS (s0, s1, s2, p1, p2);
   
---------------- COMPONENT ------------------------------------------
   
-- 7SEGMENT
	COMPONENT segment IS
      PORT( clk            : IN std_logic;                           -- clock input
            bcd            : IN std_logic_vector(3 DOWNTO 0);        -- BCD input
            segment7       : OUT std_logic_vector(6 DOWNTO 0)        -- Zahl anzeige output
            );
	END COMPONENT;
   
-- TIMER
   COMPONENT timer IS
      PORT( clk            : IN std_logic;                           -- clock input
            reset          : IN std_logic;                           -- Reset Timer
            secondout      : OUT std_logic_vector(1 DOWNTO 0);       -- Sekunden Anzeige in 2 BITs
            timeisout      : OUT std_logic                           -- '1' bei timerende
            );
   END COMPONENT;

-- STOPUHR
   COMPONENT stopwatch IS
      PORT( clk				:in std_logic;                            -- clock input
            enable			:in std_logic;                            -- Aktiviert die Stopuhr
            reset				:in std_logic;                            -- Reset Stopuhr
            centiseconds	:out integer;                             -- Ausgabe Zehntelsekunde
            seconds			:out integer;                             -- Ausgabe Sekunde
            minutes			:out integer                              -- Ausgabe Minute
            );
   END COMPONENT;
   
---------------- SIGNAL ----------------------------------------------

   SIGNAL Zustand : Zustaende;
   
   SIGNAL uebertrag                                               : std_logic_vector(n-1 DOWNTO 0);   -- Zwischeninstanz zu LED_out
   SIGNAL check                                                   : std_logic_vector(n-1 DOWNTO 0);   -- LED und SWITCH gleich?
   SIGNAL seg_0, seg_1, seg_2, seg_3, seg_4, seg_5, seg_6, seg_7  : std_logic_vector(6 DOWNTO 0);     -- für Output in der Entity
   SIGNAL bcd_0, bcd_1, bcd_2, bcd_3, bcd_4, bcd_5, bcd_6, bcd_7  : std_logic_vector(3 DOWNTO 0);     -- für Input 7Segment
   SIGNAL seconds                                                 : std_logic_vector(1 DOWNTO 0);     -- Sekundenanzeige vom Timer
   SIGNAL timeisout, reset_timer, reset_watch                     : std_logic;                        --

BEGIN

---------------- PORT MAP --------------------------------------------

   HEX_0 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_0,
               segment7    => seg_0
               );
               
   HEX_1 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_1,
               segment7    => seg_1
               );
               
   HEX_2 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_2,
               segment7    => seg_2
               );
               
   HEX_3 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_3,
               segment7    => seg_3
               );
               
   HEX_4 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_4,
               segment7    => seg_4
               );
               
   HEX_5 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_5,
               segment7    => seg_5
               );
               
   HEX_6 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_6,
               segment7    => seg_6
               );
               
   HEX_7 : segment
   PORT MAP(   clk	      => clk,
               bcd         => bcd_7,
               segment7    => seg_7
               );

   timer_0 : timer
   PORT MAP(   clk         => clk,
               reset       => reset_timer,
               secondout   => seconds,
               timeisout   => timeisout
               );

---------------- LOGIK -----------------------------------------------
   
   HEX0 <= seg_0;
   HEX1 <= seg_1;
   HEX2 <= seg_2;
   HEX3 <= seg_3;
   HEX4 <= seg_4;
   HEX5 <= seg_5;
   HEX6 <= seg_6;
   HEX7 <= seg_7;
   
   
   PROCESS(SWITCH_in, clk, reset, timeisout, seconds)
      VARIABLE zaehler, Gesamt    :  integer := 0;
      
   BEGIN
      IF(reset = '0') THEN
         Zustand <= s0;
         reset_timer <= '1';
         Gesamt      := 0;
      ELSE
         zaehler := 0;

         -- ZÄHLER ANZAHL RICHTIGE ÜBEREINSTIMMUNG --
         FOR i IN 0 TO n-1 LOOP
            check(i)    <= (uebertrag(i) AND SWITCH_in(i));
            IF(check(i) = '1') THEN
               zaehler := zaehler +1;
            END IF;
         END LOOP;
         --------------------------------------------
         
         CASE Zustand IS
   
   ---------------- STAGE 0 (START) ----------------
            WHEN s0 =>
               uebertrag   <= "000000000000000000";
               LED_out     <= uebertrag;
               reset_timer <='1';
               
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN   -- low_active
                  Zustand  <= s1;
               ELSE
                  Zustand  <= s0;
               END IF;
            
   -------------------- STAGE 1 --------------------
            WHEN s1 =>
               reset_timer <= '0';                     -- Timer aktivieren
               IF(timeisout = '0') THEN
                               -- 765432109876543210 --
                  uebertrag   <= "000000000000011111"; -- Aktive LEDs
                  LED_out     <= uebertrag;         
                  LEDG0       <= '0';                       -- LED an, wenn Timer AKTIV
               ELSE
                  LEDG0       <= '1';                       -- LED an, wenn Timer vorbei
                  reset_timer <= '1';                 -- Timer STOP and RESET
                  Gesamt      := Zaehler;         -- Addiere vorriegen Wert + aktuellen Wert
                  Zustand     <= p1;                      -- gehe in den nächsten Zustand
               END IF;

   -------------------- PAUSE 1 --------------------
            WHEN p1 =>
                  uebertrag   <= "000000000000000000";
                  LED_out     <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand     <= s2;
               ELSE
                  Zustand     <= p1;
               END IF;

   -------------------- STAGE 2 --------------------
            WHEN s2 =>
               reset_timer <= '0';                     -- Timer aktivieren
               IF(timeisout = '0') THEN
                               -- 765432109876543210 --
                  uebertrag   <= "111110000000000000"; -- Aktive LEDs
                  LED_out     <= uebertrag;         
                  LEDG0       <= '0';                       -- LED an, wenn Timer AKTIV
               ELSE
                  LEDG0       <= '1';                       -- LED an, wenn Timer vorbei
                  reset_timer <= '1';                 -- Timer STOP and RESET
                  Gesamt      := Gesamt + Zaehler;         -- Addiere vorriegen Wert + aktuellen Wert
                  Zustand     <= p2;                      -- gehe in den nächsten Zustand
               END IF;

   -------------------- PAUSE 2 --------------------
            WHEN p2 =>
                  uebertrag   <= "000000000000000000";
                  LED_out     <= uebertrag;
               IF((SWITCH_in = "000000000000000000") AND (next_step = '0')) THEN -- next_step -> high activ
                  Zustand     <= s0;
               ELSE
                  Zustand     <= p2;
               END IF;
            
            WHEN OTHERS => Zustand <= s0;
         END CASE;
         
   -------------------- ENDE --------------------
         CASE seconds IS
            WHEN "00" => bcd_0 <= "0011";  -- 0 -- to -- 3 --
            WHEN "01" => bcd_0 <= "0010";  -- 1 -- to -- 2 --
            WHEN "10" => bcd_0 <= "0001";  -- 2 -- to -- 1 --
            WHEN "11" => bcd_0 <= "0011";  -- 3 -- to -- 0 --
            WHEN OTHERS => bcd_0 <= "1111";  -- X --
         END CASE;
         
         CASE zaehler IS
            WHEN 0 => bcd_4 <= "0000";
            WHEN 1 => bcd_4 <= "0001";
            WHEN 2 => bcd_4 <= "0010";
            WHEN 3 => bcd_4 <= "0011";
            WHEN 4 => bcd_4 <= "0100";
            WHEN 5 => bcd_4 <= "0101";
            WHEN OTHERS => bcd_4 <= "0000";
         END CASE;
         
         CASE Gesamt IS
            WHEN 0   => bcd_6 <= "0000";
                        bcd_7 <= "0000";
            WHEN 1   => bcd_6 <= "0001";
                        bcd_7 <= "0000";
            WHEN 2   => bcd_6 <= "0010";
                        bcd_7 <= "0000";
            WHEN 3   => bcd_6 <= "0011";
                        bcd_7 <= "0000";
            WHEN 4   => bcd_6 <= "0100";
                        bcd_7 <= "0000";
            WHEN 5   => bcd_6 <= "0101";
                        bcd_7 <= "0000";
            WHEN 6   => bcd_6 <= "0110";
                        bcd_7 <= "0000";
            WHEN 7   => bcd_6 <= "0111";
                        bcd_7 <= "0000";
            WHEN 8   => bcd_6 <= "1000";
                        bcd_7 <= "0000";
            WHEN 9   => bcd_6 <= "1001";
                        bcd_7 <= "0000";
            WHEN 10  => bcd_6 <= "0000";
                        bcd_7 <= "0001";
            WHEN 11  => bcd_6 <= "0001";
                        bcd_7 <= "0001";
            WHEN 12  => bcd_6 <= "0010";
                        bcd_7 <= "0001";
            WHEN 13  => bcd_6 <= "0011";
                        bcd_7 <= "0001";
            WHEN 14  => bcd_6 <= "0100";
                        bcd_7 <= "0001";
            WHEN 15  => bcd_6 <= "0101";
                        bcd_7 <= "0001";
            WHEN OTHERS => bcd_6 <= "1111";
                           bcd_7 <= "1111";
         END CASE;
      END IF;
   END PROCESS;
END LOGIK;