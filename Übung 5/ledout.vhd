LIBRARY ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY ledout IS
GENERIC (
	maxrandom : integer := 15
	);

PORT (
	clear : in std_logic;
	enable : in std_logic; 
	clk: in std_logic;
	getvalue: in std_logic;
	ledout: out std_logic_vector (15 DOWNTO 0);
	randomvalue: inout integer := 0
	);
END ENTITY;

ARCHITECTURE generaterandom OF ledout is
 SIGNAL periods: integer := 0 ; --Variabel die speichert Anzahl der verlaufene Perioden
 SIGNAL randomnumberforLED: integer;
BEGIN

allLEDSout: PROCESS (clear)
BEGIN
	IF clear = '1' THEN 
	ledout <= "0000000000000000";
	END IF;
END PROCESS allLEDSout;

radnomNumberGenerate: PROCESS (enable, clk)
BEGIN
		IF enable = '1' THEN
		IF(clk'event AND clk='1') THEN
				IF (periods = maxrandom) THEN
					periods <= 0; 
				ELSE
					periods <= periods + 1 ;
				END IF;
		END IF;
		IF (getvalue'event AND getvalue = '1') THEN
			randomvalue <= periods;
		END IF;
	END IF;
	

	
END PROCESS radnomNumberGenerate;

ledout(randomvalue) <= '1';


END generaterandom;
