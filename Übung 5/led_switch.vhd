LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY led_switch IS
   GENERIC( n  : integer := 18);
   
   PORT( SWITCH   : IN  std_logic_vector (n-1 DOWNTO 0);
         sw_out   : OUT std_logic_vector (n-1 DOWNTO 0);
         LED      : OUT std_logic_vector (n-1 DOWNTO 0);
         led_in   : IN  std_logic_vector (n-1 DOWNTO 0)
         );
END led_switch;

ARCHITECTURE datastructure OF led_switch IS
BEGIN
   PROCESS(SWITCH, led_in)
   BEGIN
      FOR i IN 0 TO n-1 LOOP
         sw_out(i)   <= SWITCH(i);
         LED(i)      <= led_in(i);
      END LOOP;
   END PROCESS;
END datastructure;