LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY shift_register IS
	PORT( D							: IN std_logic;
			CLK						: IN std_logic;
			Q3, Q2, Q1, Q0			: OUT std_logic
			);
END shift_register;

ARCHITECTURE data_register OF shift_register IS
	
	COMPONENT dt_dflipflop IS
		PORT (D, CLK	: IN std_logic;
				Q, nQ		: OUT std_logic
				);
	END COMPONENT;
	
	SIGNAL s_2, s_1, s_0 : std_logic;

BEGIN

	 dflipflop3 : dt_dflipflop
	 PORT MAP ( CLK	=> CLK,
					D		=> D,
					Q		=> s_2
					);
					
	 dflipflop2 : dt_dflipflop
	 PORT MAP ( CLK	=> CLK,
					D		=> s_2,
					Q		=> s_1
					);

	 dflipflop1 : dt_dflipflop
	 PORT MAP ( CLK	=> CLK,
					D		=> s_1,
					Q		=> s_0
					);

	 dflipflop0 : dt_dflipflop
	 PORT MAP ( CLK	=> CLK,
					D		=> s_0,
					Q		=> Q0
					);
	
	Q3 <= s_2;
	Q2 <= s_1;
	Q1 <= s_0;
END data_register;