LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY tb_main IS
END tb_main;


ARCHITECTURE testbench OF tb_main IS

	COMPONENT podd_gen												-- odd Parity Generator
	PORT( X : IN std_logic_vector (2 downto 0);
			Y : OUT std_logic_vector (3 downto 0)
			);
	END COMPONENT;
	
	COMPONENT checker													-- Parity Checker
	PORT( X : IN std_logic_vector (3 downto 0);
			Y : OUT std_logic
			);
	END COMPONENT;
	
	SIGNAL	input		:	std_logic_vector (2 downto 0);	-- input
	SIGNAL	valid		:	std_logic;								-- output
	SIGNAL	connect	:	std_logic_vector (3 downto 0);	-- connection

BEGIN

	U1: podd_gen
	PORT MAP (	X => input,											-- erst Baustein => dann Signal
					Y => connect);
	
	U2: checker
	PORT MAP (	X => connect,
					Y	=> valid);

input(0) <= '0' AFTER 0ns, '1' AFTER 20ns, '0' AFTER 40ns, '1' AFTER 60ns, '0' AFTER 80ns, '1' AFTER 100ns, '0' AFTER 120ns, '1' AFTER 140ns;
input(1) <=	'0' AFTER 0ns, '1' AFTER 40ns, '0' AFTER 80ns, '1' AFTER 120ns;
input(2) <= '0' AFTER 0ns , '1' AFTER 80ns;

END testbench;
