LIBRARY ieee;
USE ieee.std_logic_1164.all; 


ENTITY checker IS
	PORT( X : IN std_logic_vector (3 downto 0);			-- from parity generator
			Y : OUT std_logic_vector (4 downto 0)			-- valide
			);
END checker;


ARCHITECTURE parity_checker OF checker IS
	SIGNAL check: std_logic;
	
	BEGIN
	check	<= (X(3) XNOR X(2)) XNOR (X(1) XNOR X(0));
	Y		<= check & X(3) & X(2) & X(1) & X(0);
	
END parity_checker;