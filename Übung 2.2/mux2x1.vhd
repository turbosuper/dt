LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY mux2x1 IS
	PORT(	EN, S0, X1, X0 : IN std_logic;
			Y : OUT std_logic
			);
END mux2x1;


ARCHITECTURE demux2x1 OF mux2x1 IS
	SIGNAL m1, m2 : bit;

	BEGIN 
	Y	<= (EN AND X1 AND S0) OR (En AND X0 AND (NOT S0));
END demux2x1;