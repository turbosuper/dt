LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY podd_gen IS
	PORT( X : IN std_logic_vector (2 downto 0);
			Y : OUT std_logic_vector (3 downto 0)
			);
END podd_gen;


ARCHITECTURE xxx OF podd_gen IS
	SIGNAL Podd: std_logic;
	
	BEGIN
	Podd	<= (X(0) XOR X(1)) XNOR X(2);
	Y		<= Podd & X;
END xxx;