LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY dt_dlatch_low IS
	PORT( D		: IN std_logic;	-- Eingang "D"
			E		: IN std_logic;	-- Eingang "Enable"
			Q, nQ	: OUT std_logic	-- Ausgaenge "Q" und "not Q"
			);
END dt_dlatch_low;

ARCHITECTURE dlatch_low OF dt_dlatch_low IS

BEGIN
	PROCESS(E, D)
	BEGIN
		IF(E = '0') THEN
			Q	<= D;
			nQ	<= NOT(D);
		ELSE
			
		END IF;
	END PROCESS;
END dlatch_low;