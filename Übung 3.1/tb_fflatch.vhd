LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY tb_fflatch IS
END tb_fflatch;

ARCHITECTURE testbench OF tb_fflatch IS

    COMPONENT dt_dlatch IS
		PORT (D, E	: IN std_logic;
				Q, nQ	: OUT std_logic
				);
    END COMPONENT;
	 
    COMPONENT dt_dflipflop IS
    PORT (D, CLK	: IN std_logic;
   		 Q, nQ	: OUT std_logic
			 );
    END COMPONENT;

    SIGNAL CLK_EN		: std_logic :='0';
    SIGNAL D			: std_logic :='0';
    SIGNAL Q_ff		: std_logic;
    SIGNAL nQ_ff		: std_logic;
    SIGNAL Q_latch	: std_logic;
    SIGNAL nQ_latch	: std_logic;

BEGIN

    dff : dt_dflipflop
    PORT MAP ( CLK	=> CLK_EN,
					D		=> D,
					Q		=> Q_ff,
   				nQ		=> nQ_ff
					);

    dlatch : dt_dlatch
    PORT MAP ( E	=> CLK_EN,
					D	=> D,
   				Q	=> Q_latch,
   				nQ	=> nQ_latch
					);

    clockstimuli : PROCESS
    BEGIN
   	 CLK_EN	<= '0'; WAIT FOR 15 ns;
   	 CLK_EN	<= '1'; WAIT FOR 15 ns;
   	 CLK_EN	<= '1'; WAIT FOR 15 ns;
    END PROCESS;
    
    datastimuli : PROCESS
    BEGIN
   	 D	<= '1'; WAIT FOR 8 ns;
   	 D <= '0'; WAIT FOR 8 ns;
   	 D <= '1'; WAIT FOR 8 ns;
    END PROCESS;

END testbench;
