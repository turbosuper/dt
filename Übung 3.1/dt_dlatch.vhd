LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY dt_dlatch IS
	PORT( D		: IN std_logic;	-- Eingang "D"
			E		: IN std_logic;	-- Eingang "Enable"
			Q, nQ	: OUT std_logic	-- Ausgaenge "Q" und "not Q"
			);
END dt_dlatch;

ARCHITECTURE dlatch OF dt_dlatch IS

BEGIN
	PROCESS(E, D)
	BEGIN
		IF(E = '1') THEN
			Q	<= D;
			nQ	<= NOT(D);
		ELSE
			
		END IF;
	END PROCESS;
END dlatch;