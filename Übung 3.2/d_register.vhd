LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY d_register IS
	PORT( D3, D2, D1, D0			: IN std_logic;
			E							: IN std_logic;
			Q3, Q2, Q1, Q0			: OUT std_logic;
			nQ3, nQ2, nQ1, nQ0	: OUT std_logic
			);
END d_register;

ARCHITECTURE data_register OF d_register IS
	
	COMPONENT dt_dlatch IS
		PORT (D, E	: IN std_logic;
				Q, nQ	: OUT std_logic
				);
	END COMPONENT;

BEGIN

	 dlatch3 : dt_dlatch
	 PORT MAP ( E		=> E,
					D		=> D3,
					Q		=> Q3,
					nQ		=> nQ3
					);
					
	 dlatch2 : dt_dlatch
	 PORT MAP ( E		=> E,
					D		=> D2,
					Q		=> Q2,
					nQ		=> nQ2
					);

	 dlatch1 : dt_dlatch
	 PORT MAP ( E		=> E,
					D		=> D1,
					Q		=> Q1,
					nQ		=> nQ1
					);

	 dlatch0 : dt_dlatch
	 PORT MAP ( E		=> E,
					D		=> D0,
					Q		=> Q0,
					nQ		=> nQ0
					);
END data_register;