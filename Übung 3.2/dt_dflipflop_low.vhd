LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY dt_dflipflop_low IS
	PORT( D, CLK	: IN std_logic;
			Q, nQ		: OUT std_logic
			);
END dt_dflipflop_low;

ARCHITECTURE dflipflop_low OF dt_dflipflop_low IS

BEGIN
	PROCESS(CLK)
	BEGIN
		IF (CLK'event AND CLK = '0') THEN
			Q	<= D;
			nQ	<= NOT(D);
		END IF;
	END PROCESS;
END dflipflop_low;