LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY dt_dflipflop IS
	PORT( D, CLK	: IN std_logic;
			Q, nQ		: OUT std_logic
			);
END dt_dflipflop;

ARCHITECTURE dflipflop OF dt_dflipflop IS

BEGIN
	PROCESS(CLK)
	BEGIN
		IF (CLK'event AND CLK = '1') THEN
			Q	<= D;
			nQ	<= NOT(D);
		END IF;
	END PROCESS;
END dflipflop;