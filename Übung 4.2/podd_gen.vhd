LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY podd_gen IS
   PORT( X : IN std_logic_vector (2 downto 0);
         Y : OUT std_logic_vector (3 downto 0)
         );
END podd_gen;

ARCHITECTURE data_pOdd OF podd_gen IS
   SIGNAL Podd: std_logic;
   SIGNAL c: std_logic;
   SIGNAL b, b0, b1, b2: boolean;

   BEGIN
      c     <= X(0) XOR X(1)  AFTER 5ns;        -- 3ns hold-time + 2ns propagation Delay
      Podd  <= c XNOR X(2)    AFTER 5ns;        -- 3ns hold-time + 2ns propagation Delay
      
      b0    <= X(0)'stable(3ns);                -- wenn X0 3ns stabil -> True
      b1    <= X(1)'stable(3ns);                -- wenn X1 3ns stabil -> True
      b2    <= X(2)'stable(3ns);                -- wenn X2 3ns stabil -> True
      b     <= (NOT b0) OR (NOT b1) or (NOT b2);-- Wenn X Signal unstabil ist, dann b -> True

   PROCESS(Podd, X, b)
   BEGIN
      IF ((X(0)'stable(3ns) OR X(1)'stable(3ns) OR X(2)'stable(3ns)) AND (NOT b)) THEN
         Y  <= Podd & X;
      ELSIF (b) then       -- wenn b unstabil
         Y  <= 'X' & X;    -- setze Podd auf "ungültig"
      ELSE                 -- zur Sicherheit (funktioniert auch ohne)
         Y  <= 'X' & X;
      END IF;
   END PROCESS;
END data_pOdd;