LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY tb_podd_generator IS
   END tb_podd_generator;

ARCHITECTURE testbench OF tb_podd_generator IS

   COMPONENT podd_gen
      PORT( X : IN std_logic_vector (2 downto 0);
            Y : OUT std_logic_vector (3 downto 0)
            );
   END COMPONENT ;

   SIGNAL X : std_logic_vector (2 downto 0);
   SIGNAL Y : std_logic_vector (3 downto 0);

   BEGIN
      podd: podd_gen
      PORT MAP(   X => X,
                  Y => Y
                  );

   datastimuli_X0 : PROCESS
   BEGIN
      X(0)  <= '0';  WAIT FOR 20 ns;
      X(0)  <= '1';  WAIT FOR 20 ns;
   END PROCESS;

   datastimuli_X1 : PROCESS
   BEGIN
      X(1)  <= '0';  WAIT FOR 40 ns;
      X(1)  <= '1';  WAIT FOR 40 ns;
   END PROCESS;

   datastimuli_X2 : PROCESS
   BEGIN
      X(2)  <= '0';  WAIT FOR 80 ns;
      X(2)  <= '1';  WAIT FOR 80 ns;
   END PROCESS;
END testbench;
