ENTITY mux2x1 is
 PORT  (En, S0, x1, x0 : in bit;
			Y1, Y2, Y3 : out bit
			);
			
END mux2x1;

ARCHITECTURE demux2x1 OF mux2x1 is

signal m1, m2 : bit;

BEGIN 
	Y1 <= (En AND x1 AND s0) OR (En AND x0 AND (NOT S0));
----------
	with S0 select
	Y2 <=  (x0 and En) when '0',
			(x1 and En) when '1';
---------			
	Y3 <= (x0 AND En) when S0 = '0' else (x1 AND En);
	
END demux2x1;
