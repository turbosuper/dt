ENTITY tb_Mux2x1 IS -- keine Schnittstellen
	END tb_Mux2x1 ;

	ARCHITECTURE testbench OF tb_Mux2x1 IS

COMPONENT mux2x1 -- Komponentendeklaration fuer Device Under Test (DUT )

PORT ( x0 , x1 : IN bit ;
		S0 : IN bit ;
		En : IN bit ;
		Y1 , Y2 , Y3 : OUT bit
);
END COMPONENT ;

SIGNAL IA : bit := '0'; -- Input Stimuli - Signale
SIGNAL IB : bit := '0';
SIGNAL S  : bit := '0';
SIGNAL E  : bit := '0';
SIGNAL Y1 : bit ; -- Output Signale
SIGNAL Y2 : bit ;
SIGNAL Y3 : bit ;
SIGNAL Q : bit;
SIGNAL	P : bit;
BEGIN

dut: mux2x1 -- Instantiieren des Device Under Test



PORT MAP ( x0 => IA,
			x1 => IB,
			S0 => S,
			En => E,
			Y1 => Y1 ,
			Y2 => Y2 ,
			Y3 => Y3 );

			P <= Y1 XOR Q;
			
IA <= '0' AFTER 0ns, '1' AFTER 25ns, '0' AFTER 50ns, '1' AFTER 75ns, '0' AFTER 100ns, '1' AFTER 125ns, '0' AFTER 150ns, '1' AFTER 175ns, '0' AFTER 200ns, '1' AFTER 225ns, '0' AFTER 250ns, '1' AFTER 275ns, '0' AFTER 300ns, '1' AFTER 325ns, '0' AFTER 350ns, '1' AFTER 375ns;
IB <=	'0' AFTER 0ns, '1' AFTER 50ns, '0' AFTER 100ns, '1' AFTER 150ns, '0' AFTER 200ns, '1' AFTER 250ns, '0' AFTER 300ns, '1' AFTER 350ns;
S <= '0' AFTER 0ns , '1' AFTER 100ns, '0' AFTER 200ns , '1' AFTER 300ns;
E <= '0' AFTER 0ns , '1' AFTER 200ns;
Q <= '0' AFTER 0ns, '0' AFTER 25ns, '0' AFTER 50ns, '0' AFTER 75ns, '0' AFTER 100ns, '0' AFTER 125ns, '0' AFTER 150ns, '0' AFTER 175ns, '0' AFTER 200ns, '1' AFTER 225ns, '0' AFTER 250ns, '1' AFTER 275ns, '0' AFTER 300ns, '0' AFTER 325ns, '1' AFTER 350ns, '1' AFTER 375ns;

END testbench ;