LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity A1_3 is
	PORT (x : in std_logic_vector (2 downto 0);
			y : out std_logic_vector (3 downto 0)
			);
			
end A1_3;

Architecture parity_gen of A1_3 is
	signal Podd : std_logic;

	begin
	Podd <= (X(0) XOR X(1)) XNOR X(2);
	Y <=  Podd & X ;
	
end parity_gen;