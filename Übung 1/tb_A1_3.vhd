LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY tb_A1_3 IS -- keine Schnittstellen
	END tb_A1_3 ;

	ARCHITECTURE testbench OF tb_A1_3 IS

COMPONENT A1_3 -- Komponentendeklaration fuer Device Under Test (DUT )

PORT (x : in std_logic_vector (2 downto 0);
			y : out std_logic_vector (3 downto 0)
			);

END COMPONENT ;

SIGNAL x : std_logic_vector (2 downto 0); -- Input Stimuli - Signale
SIGNAL y : std_logic_vector (3 downto 0) ; -- Output Signale

BEGIN

dut: A1_3 -- Instantiieren des Device Under Test

PORT MAP ( x => x,
			y => y
			);
			
x(0) <= '0' AFTER 0ns, '1' AFTER 25ns, '0' AFTER 50ns, '1' AFTER 75ns, '0' AFTER 100ns, '1' AFTER 125ns, '0' AFTER 150ns, '1' AFTER 175ns, '0' AFTER 200ns, '1' AFTER 225ns, '0' AFTER 250ns, '1' AFTER 275ns, '0' AFTER 300ns, '1' AFTER 325ns, '0' AFTER 350ns, '1' AFTER 375ns;
X(1) <=	'0' AFTER 0ns, '1' AFTER 50ns, '0' AFTER 100ns, '1' AFTER 150ns, '0' AFTER 200ns, '1' AFTER 250ns, '0' AFTER 300ns, '1' AFTER 350ns;
X(2) <= '0' AFTER 0ns , '1' AFTER 100ns, '0' AFTER 200ns , '1' AFTER 300ns;

END testbench ;