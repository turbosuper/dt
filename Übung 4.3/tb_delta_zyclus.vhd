LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY tb_delta_zyclus IS
END tb_delta_zyclus;

ARCHITECTURE testbench of tb_delta_zyclus IS

   COMPONENT delta_zyclus
      PORT(	X				: IN std_logic;
            CLK, RESET	: IN std_logic;
            Y				: OUT std_logic
            );
   END COMPONENT;
   
   SIGNAL X, CLK, RESET, Y: std_logic;

   BEGIN
      delta: delta_zyclus
      PORT MAP(   X     => X,
                  CLK   => CLK,
                  RESET => RESET,
                  Y     => Y
                  );

   datastimuli_RESET : PROCESS
   BEGIN
      RESET <= '1';  WAIT FOR 160 ps;
      RESET <= '0';  WAIT FOR 100 ns;
   END PROCESS;
   
   datastimuli_clock : PROCESS
   BEGIN
      clk   <= '0';  WAIT FOR 50 ps;
      clk   <= '1';  WAIT FOR 50 ps;
   END PROCESS;
   
   datastimuli_X : PROCESS
   BEGIN
      X     <= '0';  WAIT FOR 20 ps;
      X     <= '1';  WAIT FOR 20 ps;
   END PROCESS;
END testbench;