LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY delta_zyclus IS
   PORT( X            : IN std_logic;
         CLK, RESET   : IN std_logic;
         Y            : OUT std_logic := '0'
         );
END delta_zyclus;

ARCHITECTURE dataflow OF delta_zyclus IS
   SIGNAL a, b:   std_logic := '0';
   SIGNAL Q1, Q2: std_logic := '0';   -- Ausgaenge D-FlipFlop
   SIGNAL D1, D2: std_logic := '0';   -- Eingaenge D-FlipFlop
   
   BEGIN
      dflipflop1: PROCESS(CLK)
      BEGIN
         IF (CLK'event AND CLK = '1') THEN
            Q1 <= D1;
         END IF;
      END PROCESS;

      dflipflop2: PROCESS(CLK, RESET)
      BEGIN
         IF (RESET = '1') THEN
            Q2 <= '0';
         ELSIF (CLK'event AND CLK = '1') THEN
            Q2 <= D2;
         END IF;
      END PROCESS;
      
   Y  <= Q2;
   b  <= Q2;
   a  <= Q1;
   D1 <= X;
   D2 <= a XNOR b;
END dataflow;